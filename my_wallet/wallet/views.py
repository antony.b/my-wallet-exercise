import uuid
import datetime

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db import transaction

from .models import WalletWithdrawal, Wallet, WalletDeposit
from .serializers import WalletSerializer, WalletWithdrawalSerializer, WalletDepositSerializer


class InitializeWallet(APIView):
    @transaction.atomic
    def post(self, request):
        # In production check whether customer_xid exists in Customer service DB.
        if request.data['customer_xid'] == 'ea0212d3-abd6-406f-8c67-868e814a2436': 
            try:
                wallet = Wallet.objects.get(owned_by=request.data['customer_xid'])
                token = wallet.values()[0]['token']
                data = { "data": { "token": token }, "status": "success" }
                return Response(data, status=status.HTTP_200_OK )
            except:
                token = uuid.uuid4().hex
                Wallet.objects.create(id=token, owned_by=request.data['customer_xid'], token=token)
                wallet = Wallet.objects.filter(id=token)
                data = { "data": { "token": token }, "status": "success" }
                return Response(data, status=status.HTTP_200_OK )
        data = { "data": { "token": "nil, customer does not exist" }, "status": "failure" }
        token = uuid.uuid4().hex
        Wallet.objects.create(id=token, owned_by=request.data['customer_xid'], token=token)
        return Response(data, status=status.HTTP_400_BAD_REQUEST)

class WalletList(APIView):
    @transaction.atomic
    def post(self, request):
        try:
            authorization = request.headers['Authorization'] 
        except:
            data = { "status": "failure", "data": "Authorization token missing." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        token = authorization.split(' ')[1:2][0]
        try:
            Wallet.objects.filter(token=authorization.split(' ')[1:2][0]).update(is_disabled=False)
            query_set = Wallet.objects.filter(token=authorization.split(' ')[1:2][0])
            id = list(query_set.values())[0]['id']
            owned_by = list(query_set.values())[0]['owned_by']
            wallet_status = list(query_set.values())[0]['is_disabled']
            enabled_at = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
            balance = list(query_set.values())[0]['balance']
            data = { "status": "success", "data": { "wallet": { "id": id, "owned_by": owned_by, 
                                                    "is_disabled": wallet_status, "enabled_at": enabled_at,"balance": balance }}}
            return Response(data, status=status.HTTP_200_OK )
        except:
            data = { "status": "failure", "data": "Try with a valid token." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
    
    @transaction.atomic
    def patch(self, request):
        try:
            if request.data['is_disabled'] == 'true':
                try:
                    authorization = request.headers['Authorization'] 
                except:
                    data = { "status": "failure", "data": "Authorization token missing." }
                    return Response(data, status=status.HTTP_400_BAD_REQUEST)
                token = authorization.split(' ')[1:2][0]
                try:
                    Wallet.objects.filter(token=authorization.split(' ')[1:2][0]).update(is_disabled=True)
                    query_set = Wallet.objects.filter(token=authorization.split(' ')[1:2][0])
                    wallet_id = list(query_set.values())[0]['id']
                    owned_by = list(query_set.values())[0]['owned_by']
                    wallet_status = list(query_set.values())[0]['is_disabled']
                    disabled_at = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                    balance = list(query_set.values())[0]['balance']
                    data = { "status": "success", "data": { "wallet": { "id": wallet_id, "owned_by": owned_by, 
                                                            "disabled_at": wallet_status, "disabled_at": disabled_at,"balance": balance }}}
                    return Response(data, status=status.HTTP_200_OK )
                except:
                    data = { "status": "failure", "data": "Try with a valid token." }
                    return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except:
            data = { "status": "failure", "data": "Is_disabled flag is missing." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        try:
            authorization = request.headers['Authorization'] 
        except:
            data = { "status": "failure", "data": "Authorization token missing." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        token = authorization.split(' ')[1:2][0]
        try:
            query_set = Wallet.objects.filter(token=authorization.split(' ')[1:2][0])
            id = list(query_set.values())[0]['id']
            owned_by = list(query_set.values())[0]['owned_by']
            wallet_status = list(query_set.values())[0]['status']
            enabled_at = (list(query_set.values())[0]['enabled_at']).strftime("%m/%d/%Y, %H:%M:%S")
            balance = list(query_set.values())[0]['balance']
            data = { "status": "success", "data": { "wallet": { "id": id, "owned_by": owned_by, 
                                                    "status": wallet_status, "enabled_at": enabled_at,"balance": balance }}}
            return Response(data, status=status.HTTP_200_OK )
        except:
            data = { "status": "failure", "data": "Try with a valid token." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        
class WalletDepositList(APIView):
    @transaction.atomic
    def post(self, request):
        try:
            authorization = request.headers['Authorization']
            token = authorization.split(' ')[1:2][0]
            try:
                wallet = Wallet.objects.filter(token=token)
                amount = float(request.data['amount']) + WalletDeposit.objects.filter(wallet=wallet.values()[0]['id']).values()[0]['amount']
                WalletDeposit.objects.filter(wallet=wallet.values()[0]['id']).update(id=wallet.values()[0]['id'],
                    status=True, wallet=wallet.values()[0]['id'], deposited_by=wallet.values()[0]['owned_by'], 
                    amount=amount, reference_id=request.data['reference_id'][0])
                deposited_at = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                current_amount = WalletDeposit.objects.filter(reference_id=request.data['reference_id'][0]).values()[0]['amount']
                data = { "status": "success", "data": { "deposit": { "id": wallet.values()[0]['id'],
                                                        "deposited_by": wallet.values()[0]['owned_by'],
                                                        "status": True,
                                                        "deposited_at": deposited_at,
                                                        "amount": current_amount,
                                                        "reference_id": request.data['reference_id']}}}
                return Response(data, status=status.HTTP_200_OK )
            except:
                data = { "status": "failure", "data": "Try with a valid token." }
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except:
            data = { "status": "failure", "data": "Authorization token missing." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

class WalletWithdrawalList(APIView):
    @transaction.atomic
    def post(self, request):
        try:
            authorization = request.headers['Authorization']
            token = authorization.split(' ')[1:2][0]
            try:
                wallet = Wallet.objects.filter(token=token)
                amount = WalletDeposit.objects.filter(wallet=wallet.values()[0]['id']).values()[0]['amount'] - float(request.data['amount'])
                print(amount, '--------------------------')
                WalletDeposit.objects.filter(wallet=wallet.values()[0]['id']).update(id=wallet.values()[0]['id'], 
                    amount=amount, reference_id=request.data['reference_id'][0])
                WalletWithdrawal.objects.filter(wallet=wallet.values()[0]['id']).update(id=wallet.values()[0]['id'],
                    status=True, wallet=wallet.values()[0]['id'], withdrawn_by=wallet.values()[0]['owned_by'], 
                    amount=amount, reference_id=request.data['reference_id'][0])
                withdrawn_at = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
                current_amount = WalletDeposit.objects.filter(reference_id=request.data['reference_id'][0]).values()[0]['amount']
                data = { "status": "success", "data": { "withdrawal": { "id": wallet.values()[0]['id'],
                                                        "withdrawn_by": wallet.values()[0]['owned_by'],
                                                        "status": True,
                                                        "withdrawn_at": withdrawn_at,
                                                        "amount": current_amount,
                                                        "reference_id": request.data['reference_id']}}}
                return Response(data, status=status.HTTP_200_OK )
            except:
                data = { "status": "failure", "data": "Try with a valid token." }
                return Response(data, status=status.HTTP_400_BAD_REQUEST)
        except:
            data = { "status": "failure", "data": "Authorization token missing." }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
