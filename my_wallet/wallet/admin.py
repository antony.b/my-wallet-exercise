from django.contrib import admin
from .models import Wallet, WalletDeposit, WalletWithdrawal

admin.site.register(Wallet)
admin.site.register(WalletDeposit)
admin.site.register(WalletWithdrawal)
