from rest_framework import serializers
from .models import Wallet, WalletDeposit, WalletWithdrawal


class WalletSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Wallet
        fields = ['id', 'owned_by', 'status', 'enabled_at', 'balance', 'token']

class WalletDepositSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WalletDeposit
        fields = ['id', 'deposited_by', 'status', 'deposited_at', 'amount', 'reference_id']

class WalletWithdrawalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WalletWithdrawal
        fields = ['id', 'withdrawn_by', 'status', 'withdrawn_at', 'amount', 'reference_id']
