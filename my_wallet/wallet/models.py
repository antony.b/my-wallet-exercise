from django.db import models


class Wallet(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    owned_by = models.CharField(max_length=100)
    is_disabled = models.BooleanField(default=False)
    enabled_at = models.DateTimeField(auto_now_add=True)
    disabled_at = models.DateTimeField(auto_now_add=True)
    balance = models.FloatField(default=0)
    token = models.CharField(max_length=100)

    def __str__(self):
        return self.id

class WalletDeposit(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    deposited_by = models.CharField(max_length=100)
    status = models.BooleanField()
    deposited_at = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField(default=0)
    reference_id = models.CharField(max_length=100)

    def __str__(self):
        return self.id

class WalletWithdrawal(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    withdrawn_by = models.CharField(max_length=100)
    status = models.BooleanField()
    withdrawn_at = models.DateTimeField(auto_now_add=True)
    amount = models.FloatField(default=0)
    reference_id = models.CharField(max_length=100)

    def __str__(self):
        return self.id
