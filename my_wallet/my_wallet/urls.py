from django.contrib import admin
from django.urls import path
from wallet.views import InitializeWallet, WalletList, WalletDepositList, WalletWithdrawalList


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/init/', InitializeWallet.as_view()),
    path('api/v1/wallet/', WalletList.as_view()),
    path('api/v1/wallet/deposits/', WalletDepositList.as_view()),
    path('api/v1/wallet/withdrawals/', WalletWithdrawalList.as_view()),
]
